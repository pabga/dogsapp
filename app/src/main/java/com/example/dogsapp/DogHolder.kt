package com.example.dogsapp

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.example.dogsapp.databinding.ItemDogsBinding

class DogHolder(itemView: View): RecyclerView.ViewHolder(itemView)    {
private val binding = ItemDogsBinding.bind(itemView)


    fun bind(dogPhotoUrl: String){
        binding.imgDogItem.load(dogPhotoUrl)
    }
}