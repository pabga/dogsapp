package com.example.dogsapp

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

interface APIService {
    @GET("breed/{breed}/images")
    //fun getsDogs (@Url url:String): Response <DogResponse>
    fun getsDogsByBreed(@Path("breed") hound: String) : Call<DogResponse>


}