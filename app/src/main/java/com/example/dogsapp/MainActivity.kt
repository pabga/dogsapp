package com.example.dogsapp

import android.content.Intent
import android.os.Bundle
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.dogsapp.databinding.ActivityMainBinding
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import okhttp3.internal.notifyAll
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class MainActivity : AppCompatActivity() {
    lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        //binding.imgPrueba.load( "https://images.dog.ceo/breeds/beagle/n02088364_1128.jpg")
        binding.btSearch.setOnClickListener { search() }
        setContentView(binding.root)
        binding.textInputSearch.setHintTextColor(  20 )
    }

    private fun getRetrofit(): Retrofit {
        return Retrofit.Builder()
            .baseUrl("https://dog.ceo/api/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    private fun search() {
        val breed = binding.textInputSearch.text.toString().lowercase()
        searchByName(breed)
    }

    fun searchByName(breed: String) = GlobalScope.launch {
        val call = getRetrofit().create(APIService::class.java).getsDogsByBreed(breed).execute()
        val puppys = call.body() as DogResponse?
        // dogimages
            runOnUiThread{
                if (puppys?.status == "success") {
                launchActivity(puppys)
            }else {
                showError()
            }
            }

        hideKeyboard()
    }

    //*agregando la funcion quitar el teclado luego de presionar "buscar"
    private fun hideKeyboard() {
        val imm = getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(binding.viewRoot.windowToken, 0)

    }

    private fun showError() {
        Toast.makeText(this, "Ha ocurrido un error: Raza equivocada", Toast.LENGTH_SHORT).show()
    }

    private fun launchActivity(dogResponse: DogResponse) {
        val intent = Intent(this, DogListActivity::class.java).apply {
            putExtra(DogListActivity.DOG_RESPONSE, dogResponse)
        }
        startActivity(intent)
    }
}