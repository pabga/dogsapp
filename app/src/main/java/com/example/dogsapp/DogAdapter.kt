package com.example.dogsapp

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView

class DogAdapter (var urlDogPhotoList: List <String>) :RecyclerView.Adapter <DogHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DogHolder {
       val view = LayoutInflater
       .from(parent.context)
        .inflate(R.layout.item_dogs,parent,false)
        return DogHolder(view)
    }
    override fun onBindViewHolder(holder: DogHolder, position: Int) {
        holder.bind(urlDogPhotoList[position])
    }
    override fun getItemCount() = urlDogPhotoList.size


}