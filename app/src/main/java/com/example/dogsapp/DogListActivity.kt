package com.example.dogsapp

import android.os.Bundle
import android.widget.GridView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager

import com.example.dogsapp.databinding.ActivityDogListBinding
//const val DOG_RESPONSE = "dog_response"

class DogListActivity : AppCompatActivity() {
    lateinit var binding: ActivityDogListBinding
    lateinit var dogResponse: DogResponse

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDogListBinding.inflate(layoutInflater)

        val bundle = intent.extras
        if (bundle != null) {
            dogResponse = bundle.getParcelable<DogResponse>(DOG_RESPONSE)!!
        }
        initAdapter()
        setContentView(binding.root)
    }
    private fun initAdapter() {

        val dogAdapter = DogAdapter(dogResponse.message)
        binding.recyclerDogs.layoutManager = GridLayoutManager(this,2)
        binding.recyclerDogs.adapter = dogAdapter
    }
    companion object {
        val DOG_RESPONSE = "dog_Response"

    }
}