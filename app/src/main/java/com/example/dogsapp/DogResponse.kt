package com.example.dogsapp

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class DogResponse (
    @SerializedName ( "message") val message: List<String>,
    @SerializedName ( "status")  val status: String
    ) : Parcelable {
}